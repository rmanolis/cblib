package cblib

import (
	"crypto/rand"
	"reflect"
	"testing"
)

func TestCB(t *testing.T) {
	curves := []string{P256, P384, P521}
	sample := "this sample is for signing"
	for _, v := range curves {
		t.Log("Generate key for curve ", v)
		keys, err := GenerateKeys(v, rand.Reader)
		if err != nil {
			t.Error(err)
			return
		}
		t.Log("Generate signature")
		signature, err := Sign([]byte(sample), keys.PrivateKey, rand.Reader)
		if err != nil {
			t.Error(err)
			return
		}
		t.Log("Verify signature")
		verified, err := Verify(keys.PublicKey, []byte(sample), signature)
		if err != nil {
			t.Error(err)
			return
		}
		if !verified {
			t.Error("the signature is not valid")
			return
		}

		t.Log("Finished")
	}

}

func TestEncryptAndDecrypt(t *testing.T) {
	curves := []string{P256, P384, P521}
	for _, curve := range curves {
		keysA, err := GenerateKeys(curve, rand.Reader)
		if err != nil {
			t.Error(err)
			return
		}
		keysB, err := GenerateKeys(curve, rand.Reader)
		if err != nil {
			t.Error(err)
			return
		}
		sample := "this sample is for encryption"

		encrypted, err := Encrypt(keysA.PrivateKey, keysB.PublicKey, []byte(sample))
		if err != nil {
			t.Error(err)
			return
		}

		decrypted, err := Decrypt(keysB.PrivateKey, keysA.PublicKey, encrypted)
		if err != nil {
			t.Error(err)
			return
		}
		if !reflect.DeepEqual(decrypted, []byte(sample)) {
			t.Error("The decryption is not equal to the sample")
			return
		}
	}
}

func TestGenerateSharedSecret(t *testing.T) {
	keysA, err := GenerateKeys(P256, rand.Reader)
	if err != nil {
		t.Error(err)
		return
	}
	keysB, err := GenerateKeys(P256, rand.Reader)
	if err != nil {
		t.Error(err)
		return
	}

	sharedA, err := GenerateSharedSecret(keysA.PrivateKey, keysB.PublicKey)
	if err != nil {
		t.Error(err)
		return
	}
	sharedB, err := GenerateSharedSecret(keysB.PrivateKey, keysA.PublicKey)
	if err != nil {
		t.Error(err)
		return
	}
	if !reflect.DeepEqual(sharedA, sharedB) {
		t.Error(err)
		return
	}
}

func TestPublicKeyUnmarshalMissingCurve(t *testing.T) {
	pk := `-----BEGIN CB PUBLIC KEY-----


MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEpYIZWQlClcP4uREiXaHG5W+hQUvv
DqAYTQWVHmzPEprDR/3cAbTUaytKGNA0POHuS6NsPwYsQ+Q8Ydk7lJ3hNQ==
-----END CB PUBLIC KEY-----`
	pub := PublicKey{}
	err := pub.UnmarshalText([]byte(pk))
	if err == nil {
		t.Error("The public key passed without a curve")
		return
	}

}

func TestPublicKeyUnmarshalWrongCurve(t *testing.T) {
	pk := `-----BEGIN CB PUBLIC KEY-----
curve: P257

MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEpYIZWQlClcP4uREiXaHG5W+hQUvv
DqAYTQWVHmzPEprDR/3cAbTUaytKGNA0POHuS6NsPwYsQ+Q8Ydk7lJ3hNQ==
-----END CB PUBLIC KEY-----`
	pub := PublicKey{}
	err := pub.UnmarshalText([]byte(pk))
	if err == nil {
		t.Error("The public key passed without a correct curve")
		return
	}

}
