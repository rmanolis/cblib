package cblib

import (
	"crypto/aes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/hmac"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/pem"
	"errors"
	"io"

	"github.com/cloudflare/redoctober/padding"
	"github.com/cloudflare/redoctober/symcrypt"
)

type Keys struct {
	PublicKey  []byte
	PrivateKey []byte
}

func GenerateKeys(curve_name string, reader io.Reader) (*Keys, error) {
	pubkeyCurve, err := chooseCurve(curve_name)
	if err != nil {
		return nil, err
	}
	privatekey, err := ecdsa.GenerateKey(pubkeyCurve, reader)
	if err != nil {
		return nil, err
	}
	pk := new(PrivateKey)
	pk.fromPrivateKey(privatekey, curve_name)
	pri_bytes, err := pk.MarshalText()
	if err != nil {
		return nil, err
	}
	pubk := new(PublicKey)
	pubk.fromPublicKey(&privatekey.PublicKey, curve_name)
	pub_bytes, err := pubk.MarshalText()
	if err != nil {
		return nil, err
	}
	keys := new(Keys)
	keys.PrivateKey = pri_bytes
	keys.PublicKey = pub_bytes
	return keys, nil
}

func Verify(pub []byte, str []byte, sigb []byte) (bool, error) {
	pubk := new(PublicKey)
	err := pubk.UnmarshalText(pub)
	if err != nil {
		return false, err
	}

	sig := new(Signature)
	err = sig.UnmarshalText(sigb)
	if err != nil {
		return false, err
	}

	v := pubk.Verify(str, sig)
	return v, nil
}

func Sign(str []byte, private []byte, reader io.Reader) ([]byte, error) {
	prik := new(PrivateKey)
	err := prik.UnmarshalText(private)
	if err != nil {
		return nil, err
	}
	signature := prik.Sign(str, reader)
	return signature.MarshalText()
}

// Encrypt secures and authenticates its input using the public key
// using ECDHE with AES-128-CBC-HMAC-SHA1.
func Encrypt(ownerPrivateKey, otherPublicKey, text []byte) ([]byte, error) {
	pub := new(PublicKey)
	err := pub.UnmarshalText(otherPublicKey)
	if err != nil {
		return nil, err
	}

	shared, err := GenerateSharedSecret(ownerPrivateKey, otherPublicKey)

	iv, err := symcrypt.MakeRandom(16)
	paddedIn := padding.AddPadding(text)
	ct, err := symcrypt.EncryptCBC(paddedIn, iv, shared[:16])
	if err != nil {
		return nil, err
	}
	curve, err := chooseCurve(pub.CurveName)
	if err != nil {
		return nil, err
	}
	ephPub := elliptic.Marshal(curve, pub.X, pub.Y)
	out := make([]byte, 1+len(ephPub)+16)
	out[0] = byte(len(ephPub))
	copy(out[1:], ephPub)
	copy(out[1+len(ephPub):], iv)
	out = append(out, ct...)

	h := hmac.New(sha1.New, shared[16:])
	h.Write(iv)
	h.Write(ct)
	out = h.Sum(out)

	b := pem.Block{
		Type:    "CB ENCRYPTED TEXT",
		Bytes:   out,
		Headers: map[string]string{},
	}
	return pem.EncodeToMemory(&b), nil
}

func Decrypt(ownerPrivateKey, otherPublicKey, text []byte) ([]byte, error) {
	in := []byte{}
	bl, _ := pem.Decode(text)
	if bl != nil {
		if bl.Type != "CB ENCRYPTED TEXT" {
			return nil, errors.New("it is not the correct PEM format")
		}
		in = bl.Bytes

	} else {
		return nil, errors.New("this is not encrypted text")
	}

	shared, err := GenerateSharedSecret(ownerPrivateKey, otherPublicKey)
	if err != nil {
		return nil, err
	}

	ephLen := int(in[0])
	ct := in[1+ephLen:]
	if len(ct) < (sha1.Size + aes.BlockSize) {
		return nil, errors.New("invalid ciphertext")
	}

	tagStart := len(ct) - sha1.Size
	h := hmac.New(sha1.New, shared[16:])
	h.Write(ct[:tagStart])
	mac := h.Sum(nil)
	if !hmac.Equal(mac, ct[tagStart:]) {
		return nil, errors.New("invalid MAC")
	}

	paddedOut, err := symcrypt.DecryptCBC(ct[aes.BlockSize:tagStart], ct[:aes.BlockSize], shared[:16])
	if err != nil {
		return nil, err
	}
	out, err := padding.RemovePadding(paddedOut)
	return out, err
}

func GenerateSharedSecret(privKey, pubKey []byte) ([]byte, error) {
	prik := new(PrivateKey)
	err := prik.UnmarshalText(privKey)
	if err != nil {
		return nil, err
	}
	pubk := new(PublicKey)
	err = pubk.UnmarshalText(pubKey)
	if err != nil {
		return nil, err
	}

	if prik.CurveName != pubk.CurveName {
		return nil, errors.New("they don't have the same curve")
	}

	curv, err := chooseCurve(prik.CurveName)
	if err != nil {
		return nil, err
	}
	x, _ := curv.ScalarMult(pubk.X, pubk.Y, prik.D.Bytes())
	if x == nil {
		return nil, errors.New("failed to generate encryption key")
	}
	shared := sha256.Sum256(x.Bytes())
	return shared[:], nil
}
