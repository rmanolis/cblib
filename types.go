package cblib

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/sha256"
	"crypto/x509"
	"encoding/gob"
	"encoding/pem"
	"errors"
	"io"
	"math/big"
)

const (
	P521 = "P521"
	P384 = "P384"
	P256 = "P256"
)

// Choose P521, P384, P256
func chooseCurve(cur string) (elliptic.Curve, error) {
	if cur == P521 {
		return elliptic.P521(), nil
	} else if cur == P384 {
		return elliptic.P384(), nil
	} else if cur == P256 {
		return elliptic.P256(), nil
	}
	return nil, errors.New("no correct curve")
}

type Signature struct {
	R, S *big.Int
}

type signatureJson struct {
	r, s []byte
}

func (s *Signature) MarshalText() ([]byte, error) {
	var gb bytes.Buffer
	genc := gob.NewEncoder(&gb)
	err := genc.Encode(s)
	if err != nil {
		return nil, err
	}
	b := pem.Block{
		Type:  "CB SIGNATURE",
		Bytes: gb.Bytes(),
	}
	return pem.EncodeToMemory(&b), nil
}

func (s *Signature) UnmarshalText(text []byte) error {
	for {
		bl, r := pem.Decode(text)
		if bl != nil {

			if bl.Type == "CB SIGNATURE" {
				b := bytes.NewBuffer(bl.Bytes)
				gdec := gob.NewDecoder(b)
				err := gdec.Decode(s)
				if err != nil {
					return err
				}
				return nil
			}
		} else {
			break
		}
		text = r
	}
	return errors.New("signature block not found")
}

// PublicKey represents an ECDSA public key.
type PublicKey struct {
	X, Y      *big.Int
	CurveName string
}

func (pk *PublicKey) fromPublicKey(k *ecdsa.PublicKey, curve_name string) {
	pk.X = k.X
	pk.Y = k.Y
	pk.CurveName = curve_name
}

func (pubk *PublicKey) toPublicKey(curve elliptic.Curve) *ecdsa.PublicKey {
	pk := new(ecdsa.PublicKey)
	pk.Curve = curve
	pk.X = pubk.X
	pk.Y = pubk.Y
	return pk
}

func (p *PublicKey) MarshalText() (text []byte, err error) {
	curve, err := chooseCurve(p.CurveName)
	if err != nil {
		return nil, err
	}
	pub := ecdsa.PublicKey{Curve: curve, X: p.X, Y: p.Y}
	x, err := x509.MarshalPKIXPublicKey(&pub)
	if err != nil {
		return nil, err
	}
	b := pem.Block{
		Type:    "CB PUBLIC KEY",
		Bytes:   x,
		Headers: map[string]string{"curve": p.CurveName},
	}
	return pem.EncodeToMemory(&b), nil
}

func (p *PublicKey) UnmarshalText(text []byte) error {
	for {
		bl, r := pem.Decode(text)
		if bl != nil {
			curve, ok := bl.Headers["curve"]
			if !ok {
				return errors.New("the public key does not contain a header for the curve")
			}
			_, err := chooseCurve(curve)
			if err != nil {
				return err
			}
			if bl.Type == "CB PUBLIC KEY" {
				k, err := x509.ParsePKIXPublicKey(bl.Bytes)
				if err != nil {
					return err
				}
				p.X = k.(*ecdsa.PublicKey).X
				p.Y = k.(*ecdsa.PublicKey).Y
				p.CurveName = bl.Headers["curve"]
				return nil
			}
		} else {
			break
		}
		text = r
	}
	return errors.New("public key block not found")
}

func (p *PublicKey) IsZero() bool {
	return (p.X == nil || p.Y == nil || p.X.Int64() == 0 || p.Y.Int64() == 0)
}

func (p *PublicKey) Verify(data []byte, sign *Signature) bool {
	if p.IsZero() {
		return false
	}
	if sign == nil || sign.R == nil || sign.S == nil {
		return false
	}
	if p.X == nil || p.Y == nil {
		return false
	}
	curve, err := chooseCurve(p.CurveName)
	if err != nil {
		return false
	}
	key := ecdsa.PublicKey{
		Curve: curve,
		X:     p.X,
		Y:     p.Y,
	}
	hash := sha256.Sum256(data)
	return ecdsa.Verify(&key, hash[:], sign.R, sign.S)
}

// PrivateKey represents an ECDSA private key.
type PrivateKey struct {
	PublicKey
	D *big.Int
}

func (p *PrivateKey) Sign(data []byte, reader io.Reader) *Signature {
	curve, err := chooseCurve(p.PublicKey.CurveName)
	if err != nil {
		return nil
	}
	key := ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{Curve: curve, X: p.X, Y: p.Y},
		D:         p.D,
	}
	hash := sha256.Sum256(data)
	r, s, err := ecdsa.Sign(reader, &key, hash[:])
	if err == nil {
		return &Signature{R: r, S: s}
	}
	return nil
}

func (p *PrivateKey) MarshalText() (text []byte, err error) {
	curve, err := chooseCurve(p.PublicKey.CurveName)
	if err != nil {
		return nil, err
	}
	pri := ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{Curve: curve, X: p.X, Y: p.Y},
		D:         p.D,
	}
	x, err := x509.MarshalECPrivateKey(&pri)
	if err != nil {
		return nil, err
	}
	b := pem.Block{
		Type:    "CB PRIVATE KEY",
		Bytes:   x,
		Headers: map[string]string{"curve": p.PublicKey.CurveName},
	}
	return pem.EncodeToMemory(&b), nil
}

func (p *PrivateKey) UnmarshalText(text []byte) error {
	for {
		bl, r := pem.Decode(text)
		if bl != nil {
			if bl.Type == "CB PRIVATE KEY" {
				k, err := x509.ParseECPrivateKey(bl.Bytes)
				if err != nil {
					return err
				}
				p.D = k.D
				p.PublicKey.X = k.PublicKey.X
				p.PublicKey.Y = k.PublicKey.Y
				p.PublicKey.CurveName = bl.Headers["curve"]
				return nil
			}
		} else {
			break
		}
		text = r
	}
	return errors.New("private key block not found")
}

func (pk *PrivateKey) fromPrivateKey(k *ecdsa.PrivateKey, curve_name string) {
	pubk := new(PublicKey)
	pubk.fromPublicKey(&k.PublicKey, curve_name)
	pk.PublicKey = *pubk
	pk.D = k.D
}

func (prik *PrivateKey) toPrivateKey(curve elliptic.Curve) *ecdsa.PrivateKey {
	pk := new(ecdsa.PrivateKey)
	pk.Curve = curve
	pk.X = prik.X
	pk.Y = prik.Y
	pk.D = prik.D
	return pk
}
